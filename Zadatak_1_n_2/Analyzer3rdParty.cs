﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_n_2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data) 
        { 
            int rowCount = data.Length; 
            double[] results = new double[rowCount]; 
            for (int i = 0; i < rowCount; i++) 
            {
                results[i] = data[i].Average(); 
            } 
            return results; 
        }

        public double[] PerColumnAverage(double[][] data)
        {
            //Zadatak_1
            int columnCount = data.Rank;
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            double[] temp = new double[columnCount];
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    temp[j] = data[i][j];
                }
                results[i] = temp.Average();
                Array.Clear(temp, 0, temp.Length);
            }
            Console.WriteLine(columnCount);
            return results;

        }
    }
}
