﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_n_2
{
    interface IAnalytics 
    { 
        double[] CalculateAveragePerColumn(Dataset dataset); 
        double[] CalculateAveragePerRow(Dataset dataset); 
    }
}
